const mongoose = require('mongoose');

function authenticated(req, res, next) {
	var User = mongoose.model('user');
	User.findOne({
		token: req.token
	}).exec(function(err, data) {
		if (err || data == null) {
			return res.status(401).send({success: 'ko', msg: 'You must be authenticated to access this route.'});
		} else {
			req.user = data;
			return next();
		}
	})
}

function adminRights(req, res, next) {
	var User = mongoose.model('user');
	User.findOne({
		token: req.token,
		permission: true
	}).exec(function(err, data) {
		if (err || data == null) {
			return res.status(401).send({success: 'ko', msg: 'You must have admin right.'});
		} else {
			req.user = data;
			return next();
		}
	})
}

module.exports.authenticated = authenticated;
module.exports.adminRights = adminRights;