var mongoose = require('mongoose');

prepareMessageErrorPB = function(InformationRequest, error, ct, res, callback) {
    if (ct == error.data.length) {
        var ct2 = 0;
        var msg = "";
        while (ct2 != error.data.length) {
            if (error.data[ct2].quantity == 1) {
                msg += error.data[ct2].quantity + " exemplaire de l'objet " + error.data[ct2].name + " a déja été réservé pour le " + error.data[ct2].date.toISOString().replace(/T/, ' ').replace(/\..+/, '') + ' / '
            } else {
                msg += error.data[ct2].quantity + " exemplaires de l'objet " + error.data[ct2].name + " ont déja été réservé pour le " + error.data[ct2].date.toISOString().replace(/T/, ' ').replace(/\..+/, '') + ' / ';
            }
            ct2 += 1;
        }
        InformationRequest.err.err = true;
        InformationRequest.err.msg = msg;
        callback(InformationRequest, res);
    } else {
        var Item = mongoose.model('Item');
        Item.findOne({
           _id: error.data[ct].id
        }).exec(function(err, data) {
            if (err || data == null) {
                prepareMessageErrorPB(InformationRequest, error, ct + 1, res, callback);
            } else {
                error.data[ct].name = data.name;
                prepareMessageErrorPB(InformationRequest, error, ct + 1, res, callback);
            }
        });
    }
}

classifyNewDatePB = function(InformationListItem, hint, newDate, quantitytoAdd) {
    var tmp = {
        quantity: 0,
        quantityToAdd: quantitytoAdd,
        date: newDate
    };
    var ct = 0;
    while (ct != InformationListItem[hint].quantityPossible.length) {
        if (ct + 1 == InformationListItem[hint].quantityPossible.length) {
            InformationListItem[hint].quantityPossible.push(tmp);
            return;
        } else if (newDate < InformationListItem[hint].quantityPossible[ct + 1].date) {
            InformationListItem[hint].quantityPossible.splice(ct + 1, 0, tmp);
            return;
        } 
        ct += 1;
    }
};

checkErrorQuantityPB = function(error, InformationListItem) {
    var ct = 0;
    while (ct != InformationListItem.length) {
        var ct2 = 1;
        while (ct2 != InformationListItem[ct].quantityPossible.length) {
            var tmp = InformationListItem[ct].quantityPossible[0];
            
            tmp.quantity -= InformationListItem[ct].quantityPossible[ct2].quantityToAdd;
            ct2 += 1;
        }
        ct += 1;
    }
}

calculateQuantityPB = function(InformationListItem) {
    var ct = 0;
    while (ct != InformationListItem.length) {
        var ct2 = 1;
        while (ct2 != InformationListItem[ct].quantityPossible.length) {
            var tmp = InformationListItem[ct].quantityPossible[0];
            tmp.quantity -= InformationListItem[ct].quantityPossible[ct2].quantityToAdd;
            ct2 += 1;
        }
        ct += 1;
    }
    ct = 0;
    while (ct != InformationListItem.length) {
        var ct2 = 1;
        while (ct2 != InformationListItem[ct].quantityPossible.length) {
            var tmp = InformationListItem[ct].quantityPossible[ct2];
            tmp.quantity = InformationListItem[ct].quantityPossible[ct2 - 1].quantity + tmp.quantityToAdd;
            ct2 += 1;
        }
        ct += 1;
    }
}

checkDateErrorPB = function (error, InformationListItem, hint, dateStart, dateEnd, quantity) {
    var ct = 0;
    var state = false;

    while (ct != InformationListItem[hint].quantityPossible.length) {
        if (InformationListItem[hint].quantityPossible[ct].date > dateStart) {
            if (InformationListItem[hint].quantityPossible[ct - 1].quantity < quantity) {
                error.data.push({
                    id: InformationListItem[hint].item,
                    quantity: quantity - InformationListItem[hint].quantityPossible[ct - 1].quantity,
                    date: dateStart,
                    name: ""
                });
                error.error = true;
                state = true;
                break;
            } else {
                this.classifyNewDatePB(InformationListItem, hint, dateStart, -quantity);
                if (dateEnd != null) {
                    this.classifyNewDatePB(InformationListItem, hint, dateEnd, quantity);
                }
                this.calculateQuantityPB(InformationListItem);
                state = true;
                break;
            }
        }
        ct += 1;
    }
    if (!state) {
        if (InformationListItem[hint].quantityPossible[ct - 1].quantity < quantity) {
            error.data.push({
                id: InformationListItem[hint].item,
                quantity: quantity - InformationListItem[hint].quantityPossible[ct - 1].quantity,
                date: dateStart,
                name: ""
            });
            error.error = true;
        } else {
            this.classifyNewDatePB(InformationListItem, hint, dateStart, -quantity);
            if (dateEnd != null) {
                this.classifyNewDatePB(InformationListItem, hint, dateEnd, quantity);
            }
            this.calculateQuantityPB(InformationListItem);
        }
    }
}

lookItemInData2PB = function(error, InformationListItem, hint, data) {
    var ct = 0;
    while (ct != data.length) {
        if (data[ct].idPack != null) {
            var ct2 = 0;
            while (ct2 != data[ct].idPack.items.length) {
                if (data[ct].idPack.items[ct2].equals(InformationListItem[hint].item)) {
                    this.checkDateErrorPB(error, InformationListItem, hint, data[ct].dateStart, data[ct].dateEnd, data[ct].idPack.quantity[ct2]);
                    break;
                }
                ct2 += 1;
            }
        }
        ct += 1;
    }
}

FirstCheckErrorPackBorrowPB = function(error, InformationRequest, InformationListItem, hint, res, callback) {
    var PackBorrow = mongoose.model('packBorrow');
    PackBorrow.find({
        dateEnd: {$lte: InformationRequest.endDate},
        dateStart: {$gte: InformationRequest.startDate, $lte: InformationRequest.endDate},
        State: {$ne: 3}
    }).populate({
        path: 'idPack',
        match: {items: {$all: [InformationListItem[hint].item]}}
    }).exec(function(err, data) {
        if (err) {
            InformationRequest.err.err = true;
            InformationRequest.err.msg = err;
            callback(InformationRequest, res);
        } else if (data.length == 0) {
            this.continueFirstCheckErrorPB(error, InformationRequest, InformationListItem, hint + 1, res, callback);
        } else {
            this.lookItemInData2PB(error, InformationListItem, hint, data);
            this.continueFirstCheckErrorPB(error, InformationRequest, InformationListItem, hint + 1, res, callback)
        }
    });
}

SecondCheckErrorPackBorrowPB = function(error, InformationRequest, InformationListItem, hint, res, callback) {
    var PackBorrow = mongoose.model('packBorrow');
    PackBorrow.find({
        dateEnd: {$gte: InformationRequest.endDate},
        dateStart: {$gte: InformationRequest.startDate, $lte: InformationRequest.endDate},
        State: {$ne: 3}
    }).populate({
        path: 'idPack',
        match: {items: {$all: [InformationListItem[hint].item]}}
    }).exec(function(err, data) {
        if (err) {
            InformationRequest.err.err = true;
            InformationRequest.err.msg = err;
            callback(InformationRequest, res);
        } else if (data.length == 0) {
            this.continueSecondCheckErrorPB(error, InformationRequest, InformationListItem, hint + 1, res, callback);
        } else {
            this.lookItemInData2PB(error, InformationListItem, hint, data);
            this.continueSecondCheckErrorPB(error, InformationRequest, InformationListItem, hint + 1, res, callback);
        }
    });
}

verifyTotalItemQuantityPB = function(InformationRequest, InformationListItem, ct, res, callback) {
    var Item = mongoose.model('Item');
    Item.findOne({
        _id: InformationListItem[ct].item,
    }).exec(function(err, data) {
        if (err) {
            InformationRequest.err.err = true;
            InformationRequest.err.msg = err;
            callback(InformationRequest, res);
        } else if (data == null) {
            this.startVerificationPB(true, InformationRequest, InformationListItem, ct, res, callback);
        } else {
            InformationListItem[ct].quantityPossible.push({
                quantity: data.totalQuantity - InformationListItem[ct].quantityAsk,
                quantityToAdd: 0,
                date: InformationRequest.startDate
            });
            this.startVerificationPB(false, InformationRequest, InformationListItem, ct + 1, res, callback);
        }
    });
}

verifyItemBorrowQuantityPB = function(InformationRequest, InformationListItem, hint, res, callback) {
    var ItemBorrow = mongoose.model('itemBorrow');
    ItemBorrow.find({
        itemInfo: {$all: [InformationListItem[hint].item]},
        dateEnd: {$gte: InformationRequest.startDate},
    }).exec(function(err, data) {
        if (err) {
            InformationRequest.err.err = true;
            InformationRequest.err.msg = err;
            callback(InformationRequest, res);
        } else if (data.length == 0) {
            this.continueVerificationItemBorrowPB(InformationRequest, InformationListItem, hint + 1, res, callback);
        } else {
            var ct = 0;
            while (ct != data.length) {
                var ct2 = 0;
                while (ct2 != data[ct].itemInfo.length) {
                    if (data[ct].itemInfo[ct2].equals(InformationListItem[hint].item)) {
                        this.classifyNewDatePB(InformationListItem, hint, data[ct].dateEnd, data[ct].quantity[ct2]);
                    }
                    ct2 += 1;
                }
                ct += 1;
            }
            this.continueVerificationItemBorrowPB(InformationRequest, InformationListItem, hint + 1, res, callback);
        }
    });
}

lookItemInDataPB = function(InformationListItem, hint, data) {
    var ct = 0;
    while (ct != data.length) {
        if (data[ct].idPack != null) {
            var ct2 = 0;
            while (ct2 != data[ct].idPack.items.length) {
                if (data[ct].idPack.items[ct2].equals(InformationListItem[hint].item)) {
                    this.classifyNewDatePB(InformationListItem, hint, data[ct].dateEnd, data[ct].idPack.quantity[ct2]);
                    break;
                }
                ct2 += 1;
            }
        }
        ct += 1;
    }
}

verifyPackBorrowQuantityPB = function(InformationRequest, InformationListItem, hint, res, callback) {
    var PackBorrow = mongoose.model('packBorrow');
    PackBorrow.find({
        dateEnd: {$gte: InformationRequest.startDate},
        dateStart: {$lte: InformationRequest.startDate},
        State: {$ne: 3}
    }).populate({
        path: 'idPack',
        match: {items: {$all: [InformationListItem[hint].item]}}
    }).exec(function(err, data) {
        if (err) {
            InformationRequest.err.err = true;
            InformationRequest.err.msg = err;
            callback(InformationRequest, res);
        } else if (data.length == 0) {
            continueVerificationPackBorrowPB(InformationRequest, InformationListItem, hint + 1, res, callback);
        } else {
            lookItemInDataPB(InformationListItem, hint, data);
            continueVerificationPackBorrowPB(InformationRequest, InformationListItem, hint + 1, res, callback);
        } 
    })
}

continueSecondCheckErrorPB = function(error, InformationRequest, InformationListItem, ct, res, callback) {
    if (error.error == true && ct == InformationListItem.length) {
        prepareMessageErrorPB(InformationRequest, error, 0, res, callback)
    } else if (error.error == false && ct == InformationListItem.length) {
        callback(InformationRequest, res);
    } else {
        this.SecondCheckErrorPackBorrowPB(error, InformationRequest, InformationListItem, ct, res, callback);
    }
}

continueFirstCheckErrorPB = function(error, InformationRequest, InformationListItem, ct, res, callback) {
    if (error.error == true && ct == InformationListItem.length) {
        prepareMessageErrorPB(InformationRequest, error, 0, res, callback)
    } else if (error.error == false && ct == InformationListItem.length) {
        this.continueSecondCheckErrorPB(error, InformationRequest, InformationListItem, 0, res, callback);
    } else {
        this.FirstCheckErrorPackBorrowPB(error, InformationRequest, InformationListItem, ct, res, callback);
    }
}


checkErrorOriginQuantity = function(error, InformationRequest, InformationListItem, ct, res, callback) {
    if (error.error == true && ct == InformationListItem.length) {
        prepareMessageErrorPB(InformationRequest, error, 0, res, callback);
    } else if (error.error == false && ct == InformationListItem.length) {
        this.continueFirstCheckErrorPB(error, InformationRequest, InformationListItem, 0, res, callback);
    } else {
        if (InformationListItem[ct].quantityPossible[0].quantity < 0) {
            error.error = true;
            error.data.push({
                id: InformationListItem[ct].item,
                quantity: -InformationListItem[ct].quantityPossible[0].quantity,
                date: InformationListItem[ct].quantityPossible[0].date,
                name: ""
            });
        }
        checkErrorOriginQuantity(error, InformationRequest, InformationListItem, ct + 1, res, callback);
    }
}

continueVerificationPackBorrowPB = function(InformationRequest, InformationListItem, ct, res, callback) {
    if (ct == InformationListItem.length) {
        this.calculateQuantityPB(InformationListItem);
        var error = {
            error: false,
            data: []
        };
        this.checkErrorOriginQuantity(error, InformationRequest, InformationListItem, 0, res, callback);
    } else {
        this.verifyPackBorrowQuantityPB(InformationRequest, InformationListItem, ct, res, callback);
    }
}

continueVerificationItemBorrowPB = function(InformationRequest, InformationListItem, ct, res, callback) {
    if (ct == InformationListItem.length) {
        this.continueVerificationPackBorrowPB(InformationRequest, InformationListItem, 0, res, callback);
    } else {
        verifyItemBorrowQuantityPB(InformationRequest, InformationListItem, ct, res, callback);
    }
}

startVerificationPB = function(err, InformationRequest, InformationListItem, ct, res, callback) {
    if (err) {
        InformationRequest.err.err = true;
        InformationRequest.err.msg = 'error on Items : ' + InformationListItem[ct].item;
        callback(InformationRequest, res);
    } else {
        if (ct == InformationListItem.length) {
            this.continueVerificationItemBorrowPB(InformationRequest, InformationListItem, 0, res, callback);
        } else {
            verifyTotalItemQuantityPB(InformationRequest, InformationListItem, ct, res, callback);
        }
    }
}

module.exports.createInformationListItem = function(InformationRequest, res, callback) {
    var Pack = mongoose.model('pack');
    Pack.findOne({
        _id: InformationRequest.idPack
    }).exec(function(err, data) {
        if (err) {
            InformationRequest.err.err = true;
            InformationRequest.err.msg = err;
            callback(InformationRequest, res);
        } else if (data == null) {
            InformationRequest.err.err = true;
            InformationRequest.err.msg = 'Error on id Pack';
            callback(InformationRequest, res);
        } else {
            var InformationListItem = [];
            var ct = 0;
            while (ct != data.items.length) {
                InformationListItem.push({
                    item: data.items[ct],
                    quantityAsk: data.quantity[ct],
                    quantityPossible: []
                })
                ct += 1;
            }
            this.startVerificationPB(err, InformationRequest, InformationListItem, 0, res, callback);
        }
    })
}