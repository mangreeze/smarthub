var mongoose = require('mongoose');

var ItemBorrow = require('../models/itemsBorrow');

prepareMessageError = function(error, ct, res, callback) {
    if (ct == error.data.length) {
        return res.status(400).send({success: 'ko', msg: error.data});
    } else {
        var Item = mongoose.model('Item');
        Item.findOne({
           _id: error.data[ct].id
        }).exec(function(err, data) {
            if (err || data == null) {
                callback(error, ct + 1, res, callback);
            } else {
                error.data[ct].name = data.name;
                callback(error, ct + 1, res, callback);
            }
        });
    }
}

classifyNewDate = function(InformationListItem, hint, newDate, quantitytoAdd) {
    var tmp = {
        quantity: 0,
        quantityToAdd: quantitytoAdd,
        date: newDate
    };
    var ct = 0;
    while (ct != InformationListItem[hint].quantityPossible.length) {
        if (ct + 1 == InformationListItem[hint].quantityPossible.length) {
            InformationListItem[hint].quantityPossible.push(tmp);
            return;
        } else if (newDate < InformationListItem[hint].quantityPossible[ct + 1].date) {
            InformationListItem[hint].quantityPossible.splice(ct + 1, 0, tmp);
            return;
        } 
        ct += 1;
    }
};

calculateQuantity = function(InformationListItem) {
    var ct = 0;

    while (ct != InformationListItem.length) {
        var ct2 = 1;
        while (ct2 != InformationListItem[ct].quantityPossible.length) {
            var tmp = InformationListItem[ct].quantityPossible[ct2];
            tmp.quantity = InformationListItem[ct].quantityPossible[ct2 - 1].quantity + tmp.quantityToAdd;
            ct2 += 1;
        }
        ct += 1;
    }
}

checkDateError = function (error, InformationListItem, hint, dateStart, dateEnd, quantity) {
    var ct = 0;
    var state = false;

    while (ct != InformationListItem[hint].quantityPossible.length) {
        if (InformationListItem[hint].quantityPossible[ct].date > dateStart) {
            if (InformationListItem[hint].quantityPossible[ct - 1].quantity < quantity) {
                error.data.push({
                    id: InformationListItem[hint].item,
                    quantity: quantity - InformationListItem[hint].quantityPossible[ct - 1].quantity,
                    date: dateStart
                });
                error.error = true;
                state = true;
                break;
            } else {
                this.classifyNewDate(InformationListItem, hint, dateStart, -quantity);
                if (dateEnd != null) {
                    this.classifyNewDate(InformationListItem, hint, dateEnd, quantity);
                }
                this.calculateQuantity(InformationListItem);
                state = true;
                break;
            }
        }
        ct += 1;
    }
    if (!state) {
        if (InformationListItem[hint].quantityPossible[ct - 1].quantity < quantity) {
            error.data.push({
                id: InformationListItem[hint].item,
                quantity: quantity - InformationListItem[hint].quantityPossible[ct - 1].quantity,
                date: dateStart
            });
            error.error = true;
        } else {
            this.classifyNewDate(InformationListItem, hint, dateStart, -quantity);
            if (dateEnd != null) {
                this.classifyNewDate(InformationListItem, hint, dateEnd, quantity);
            }
            this.calculateQuantity(InformationListItem);
        }
    }
}

lookItemInData2 = function(error, InformationListItem, hint, data) {
    var ct = 0;
    while (ct != data.length) {
        if (data[ct].idPack != null) {
            var ct2 = 0;
            while (ct2 != data[ct].idPack.items.length) {
                if (data[ct].idPack.items[ct2].equals(InformationListItem[hint].item)) {
                    this.checkDateError(error, InformationListItem, hint, data[ct].dateStart, data[ct].dateEnd, data[ct].idPack.quantity[ct2]);
                    break;
                }
                ct2 += 1;
            }
        }
        ct += 1;
    }
}

FirstCheckErrorPackBorrow = function(error, duration, InformationListItem, hint, res, callback) {
    var PackBorrow = mongoose.model('packBorrow');
    var endDate = new Date();
    endDate.setHours(endDate.getHours() + duration);
    PackBorrow.find({
        dateEnd: {$lte: endDate},
        dateStart: {$gte: new Date(), $lte: endDate},
        State: 0
    }).populate({
        path: 'idPack',
        match: {items: {$all: [InformationListItem[hint].item]}}
    }).exec(function(err, data) {
        if (err || data.length == 0) {
            callback(error, duration, InformationListItem, hint + 1, res);
        } else {
            this.lookItemInData2(error, InformationListItem, hint, data);
            callback(error, duration, InformationListItem, hint + 1, res)
        }
    });
}

SecondCheckErrorPackBorrow = function(error, duration, InformationListItem, hint, res, callback) {
    var PackBorrow = mongoose.model('packBorrow');
    var endDate = new Date();
    endDate.setHours(endDate.getHours() + duration);
    PackBorrow.find({
        dateEnd: {$gte: endDate},
        dateStart: {$gte: new Date(), $lte: endDate},
        State: 0
    }).populate({
        path: 'idPack',
        match: {items: {$all: [InformationListItem[hint].item]}}
    }).exec(function(err, data) {
        if (err || data.length == 0) {
            callback(error, duration, InformationListItem, hint + 1, res);
        } else {
            this.lookItemInData2(error, InformationListItem, hint, data);
            callback(error, duration, InformationListItem, hint + 1, res);
        }
    });
}

verifyCurrentItemQuantity = function(itemTable, duration, InformationListItem, ct, res, callback) {
    var Item = mongoose.model('Item');
    Item.findOne({
        _id: itemTable[ct].id,
        currentQuantity: {$gte: itemTable[ct].quantity}
    }).exec(function(err, data) {
        if (err || data == null) {
            callback(true, itemTable, duration, InformationListItem, ct, res);
        } else {
            InformationListItem[ct].quantityPossible.push({
                quantity: data.currentQuantity - itemTable[ct].quantity,
                quantityToAdd: 0,
                date: new Date()
            });
            callback(false, itemTable, duration, InformationListItem, ct + 1, res);
        }
    });
}

verifyItemBorrowQuantity = function(duration, InformationListItem, hint, res, callback) {
    var ItemBorrow = mongoose.model('itemBorrow');
    var tmp = new Date();
    ItemBorrow.find({
        itemInfo: {$all: [InformationListItem[hint].item]},
        dateEnd: {$gte: new Date(), $lte: new Date(tmp.setHours(tmp.getHours() + duration))}
    }).exec(function(err, data) {
        if (err || data.length == 0) {
            callback(duration, InformationListItem, hint + 1, res);
        } else {
            var ct = 0;
            while (ct != data.length) {
                var ct2 = 0;
                while (ct2 != data[ct].itemInfo.length) {
                    if (data[ct].itemInfo[ct2].equals(InformationListItem[hint].item)) {
                        this.classifyNewDate(InformationListItem, hint, data[ct].dateEnd, data[ct].quantity[ct2]);
                    }
                    ct2 += 1;
                }
                ct += 1;
            }
            callback(duration, InformationListItem, hint + 1, res);
        }
    });
}

lookItemInData = function(InformationListItem, hint, data) {
    var ct = 0;
    while (ct != data.length) {
        if (data[ct].idPack != null) {
            var ct2 = 0;
            while (ct2 != data[ct].idPack.items.length) {
                if (data[ct].idPack.items[ct2].equals(InformationListItem[hint].item)) {
                    this.classifyNewDate(InformationListItem, hint, data[ct].dateEnd, data[ct].idPack.quantity[ct2]);
                    break;
                }
                ct2 += 1;
            }
        }
        ct += 1;
    }
}

verifyPackBorrowQuantity = function(duration, InformationListItem, hint, res, callback) {
    var PackBorrow = mongoose.model('packBorrow');
    var tmp = new Date();
    PackBorrow.find({
        dateEnd: {$lte: tmp.setHours(tmp.getHours() + duration)},
        State: 1
    }).populate({
        path: 'idPack',
        match: {items: {$all: [InformationListItem[hint].item]}}
    }).exec(function(err, data) {
        if (err || data.length == 0) {
            callback(duration, InformationListItem, hint + 1, res);
        } else {
            this.lookItemInData(InformationListItem, hint, data);
            callback(duration, InformationListItem, hint + 1, res);
        } 
    })
}

continueSecondCheckError = function(error, duration, InformationListItem, ct, res) {
    if (error.error == true && ct == InformationListItem.length) {
        prepareMessageError(error, 0, res, prepareMessageError)
    } else if (error.error == false && ct == InformationListItem.length) {
        ItemBorrow.createBorrowItems(InformationListItem, duration, res); 
    } else {
        this.SecondCheckErrorPackBorrow(error, duration, InformationListItem, ct, res, this.continueSecondCheckError);
    }
}

continueFirstCheckError = function(error, duration, InformationListItem, ct, res) {
    if (error.error == true && ct == InformationListItem.length) {
        prepareMessageError(error, 0, res, prepareMessageError)
    } else if (error.error == false && ct == InformationListItem.length) {
        this.continueSecondCheckError(error, duration, InformationListItem, 0, res);
    } else {
        this.FirstCheckErrorPackBorrow(error, duration, InformationListItem, ct, res, this.continueFirstCheckError);
    }
}

continueVerificationPackBorrow = function(duration, InformationListItem, ct, res) {
    if (ct == InformationListItem.length) {
        this.calculateQuantity(InformationListItem);
        var error = {
            error: false,
            data: []
        };
        this.continueFirstCheckError(error, duration, InformationListItem, 0, res);
    } else {
        this.verifyPackBorrowQuantity(duration, InformationListItem, ct, res, this.continueVerificationPackBorrow);
    }
}

continueVerificationItemBorrow = function(duration, InformationListItem, ct, res) {
    if (ct == InformationListItem.length) {
        this.continueVerificationPackBorrow(duration, InformationListItem, 0, res);
    } else {
        this.verifyItemBorrowQuantity(duration, InformationListItem, ct, res, this.continueVerificationItemBorrow);
    }
}

module.exports.startVerification = function(err, itemTable, duration, InformationListItem, ct, res) {
    if (err) {
        return res.status(400).send({success: 'ko', msg: 'error on Items : ' + itemTable[ct].id});
    } else {
        if (ct == itemTable.length) {
            this.continueVerificationItemBorrow(duration, InformationListItem, 0, res);
        } else {
            InformationListItem.push({
                item: itemTable[ct].id,
                quantityAsk: itemTable[ct].quantity,
                quantityPossible: []
            });
            verifyCurrentItemQuantity(itemTable, duration, InformationListItem, ct, res, module.exports.startVerification);
        }
    }
}