const express = require('express');
const cors = require('cors');
//const path = require('path');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const bearerToken = require('express-bearer-token');

const users = require('./routes/users');
const items = require('./routes/items');
const pack = require('./routes/pack');
const recoveryPassword = require('./routes/recovery-password');

const app = express();

var mongoDB = 'mongodb://127.0.0.1/smarthub';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(cors({credentials: true, origin: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({secret: 'smarthub_super_secret', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('image'));
app.use(bearerToken());

app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.')
        , root    = namespace.shift()
        , formParam = root;
  
      while(namespace.length) {
        formParam += '[' + namespace.shift() + ']';
      }
      return {
        param : formParam,
        msg   : msg,
        value : value
      };
    }
}));

app.use('/api/users', users);
app.use('/api/items', items);
app.use('/api/pack', pack);
app.use('/api/recovery-password', recoveryPassword);
app.set('port', (process.env.PORT || 8040));

app.listen(app.get('port'), function() {
    console.log('Server started on port ' + app.get('port'));
});


