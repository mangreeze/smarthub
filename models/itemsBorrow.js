var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Item = require('./items');

var ItemBorrowSchema = new mongoose.Schema({
    itemInfo: [{type: Schema.Types.ObjectId, ref: Item}],
    quantity: [Number],
    dateStart: Date,
    dateEnd: Date
});

var ItemBorrow = module.exports = mongoose.model('itemBorrow', ItemBorrowSchema);

module.exports.createBorrowItems = function(InformationListItem, duration, res) {
    var ct = 0;
    var tmp = new Date();
    var newItemBorrow = new ItemBorrow;
    while (ct != InformationListItem.length) {
        newItemBorrow.itemInfo.push(InformationListItem[ct].item);
        newItemBorrow.quantity.push(InformationListItem[ct].quantityAsk);
        Item.borrowItems(InformationListItem[ct].item, InformationListItem[ct].quantityAsk);
        ct += 1;
    }
    newItemBorrow.dateStart = new Date(tmp);
    newItemBorrow.dateEnd = new Date(tmp.setHours(tmp.getHours() + duration));
    newItemBorrow.save();
    res.status(200).send({success: 'ok', msg: 'Items has been borrow'});
}

module.exports.verifyBorrowing = function() {
    var tmp = ItemBorrow.find({
        dateEnd: {$lte: Date()}
    }).exec(function(err, count) {
        if (count.length != 0) {
            var ct = 0;
            while (ct != count.length) {
                var ct2 = 0;
                while (ct2 != count[ct].itemInfo.length && ct2 != count[ct].quantity.length) {
                    Item.givaBackItems(count[ct].itemInfo[ct2], count[ct].quantity[ct2]);
                    ct2 += 1;
                }
                count[0].remove();
                ct += 1;
            }
        }
    });
}

setInterval(ItemBorrow.verifyBorrowing, 60000);
