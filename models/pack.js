var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Items = require('./items');
const User = require('./user');

var PackSchema = new mongoose.Schema({
    name: String,
    user: {type: Schema.Types.ObjectId, ref: User},
    items: [{type: Schema.Types.ObjectId, ref: Items}],
    quantity: [Number],
    warning: [Boolean]
});

var Pack = module.exports = mongoose.model('pack', PackSchema);

module.exports.registerPack = function(name, id, itemsArray, res) {
    var ct = 0;
    var newPack = new Pack;
    newPack.name = name;
    newPack.user = id;
    while (ct != itemsArray.length) {
        newPack.items.push(itemsArray[ct].id);
        newPack.quantity.push(itemsArray[ct].quantity);
        newPack.warning.push(false);
        ct += 1;
    }
    newPack.save();
    res.status(200).send({success: 'ok', msg: 'Pack has been created'});
}

module.exports.createPack = function(err, req, res, ct) {
    if (err) {
        return res.status(400).send({success: 'ko', msg: 'error on Items'});
    } else {
        if (ct == req.body.items.length) {
            module.exports.registerPack(req.body.name, req.user._id, req.body.items, res);
        } else {
            Items.verifyItems(req, res, ct, Pack.createPack);
        }
    }
}

module.exports.modifyInformationPack = function(name, itemsArray, id, user, res) {
    Pack.findOne({
        _id: id
    }).exec(function(err, count) {
        if (err || count == null) {
            return res.status(400).send({success: 'ko', msg: 'error on id'});
        } else if (count.user !== user._id && user.permission == false) {
            return res.status(400).send({success: 'ko', msg: 'Permission denied'});
        } else {
            var ct = 0;
            count.name = name;
            count.items.splice(0, count.items.length);
            count.quantity.splice(0, count.quantity.length);
            count.warning.splice(0, count.warning.length);
            while (ct != itemsArray.length) {
                count.items.push(itemsArray[ct].id);
                count.quantity.push(itemsArray[ct].quantity);
                count.warning.push(false);
                ct += 1;
            }
            count.save();
            res.status(200).send({success: 'ok', msg: 'Pack has been modified'});
        }
    });
}

module.exports.modifyPack = function(err, req, res, ct) {
    if (err) {
        return res.status(400).send({success: 'ko', msg: 'error on Items'});
    } else {
        if (ct == req.body.items.length) {
            return module.exports.modifyInformationPack(req.body.name, req.body.items, req.params.id, req.user, res)
        } else {
            Items.verifyItems(req, res, ct, Pack.modifyPack);
        }
    }
}

module.exports.erasePack = function(idPack, req, res) {
    Pack.findOne({
        _id: idPack
    }).exec(function(err, count) {
        if (err || count == null) {
            return res.status(400).send({success: 'ko', msg: 'error on id'});
        } else if (count.user !== req.user._id && req.user.permission == false) {
            return res.status(401).send({success: 'ko', msg: 'Don \'t have permission'});
        } else {
            count.remove();
            res.status(200).send({success: 'ok', msg: 'Pack has been erased'});
        }
    });
}

module.exports.getInformationOfPack = function(tmp, res) {
    tmp.exec(function (err, data) {
        if (err) {
            return res.status(400).send({success: 'ko', msg: 'Error on exec'});
        } else {
            var ct = 0;
            var result = [];
            while (ct != data.length) {
                var tmp = {};
                tmp.id = data[ct]._id;
                tmp.name = data[ct].name;
                tmp.user = data[ct].user.email;
                var ct2 = 0;
                var table = [];
                while (ct2 != data[ct].items.length) {
                    table.push({id: data[ct].items[ct2]._id, name: data[ct].items[ct2].name, quantity: data[ct].quantity[ct2]});
                    ct2 += 1;
                }
                tmp.items = table;
                result.push(tmp);
                ct += 1;
            }
            return res.status(200).send({success: 'ok', msg: result});
        }
    });
}

module.exports.getAllPack = function(res) {
    var tmp = Pack.find({}).populate('items').populate('user');
    return module.exports.getInformationOfPack(tmp, res);
}

module.exports.getPack = function(req, res) {
    if (req.params.id !== req.user._id && req.user.permission == false) {
        return res.status(401).send({success: 'ko', msg: 'Permission denied'});
    }
    var tmp = Pack.find({user: req.params.id}).populate('items');
    return Pack.getInformationOfPack(tmp, res);
}
