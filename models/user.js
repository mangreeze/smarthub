var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

var UserSchema = new mongoose.Schema({
    email: {type: String, index: true},
    password: String,
    permission: Boolean,
    token: String
});

var User = module.exports = mongoose.model('user', UserSchema);

module.exports.createUser = function(req, res) {
    var newUser = new User({
        email: req.body.email,
        password: req.body.password,
        permission: req.body.permission
    });
    var tmp = User.find({email : newUser.email});
    tmp.exec(function(err, count) {
        if (count.length == 0) {
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(newUser.password, salt, function(err, hash) {
              newUser.password = hash;
              newUser.save();
              res.status(200).send({success: 'ok', msg: 'New account register'});
            });
          });
        } else {
          res.status(400).send({success: 'ko', msg: 'The account already exist'});
        }
    });
}

module.exports.getUserByEmail = function(email, callback) {
    var tmp = {email: email};
    User.findOne(tmp, callback);
};

module.exports.getUserById = function(id, callback) {
    User.findById(id, callback);
};

module.exports.comparePassword = function(candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
      if (err) {throw err;}
      callback(null, isMatch);
    });
};

module.exports.createFirstAdminAccount = function(req, res) {
    User.find({}).exec(function(err, count) {
        if (count.length == 0) {
            var newUser = new User ({
            email: req.body.email,
            password: req.body.password,
            permission: true
            });
            bcrypt.genSalt(10, function(err, salt) {
                bcrypt.hash(newUser.password, salt, function(err, hash) {
                    newUser.password = hash;
                    newUser.save();
                    res.status(201).send({success: 'ok', msg: 'Admin account has been created'});
                });
            });
        } else {
            res.status(400).send({success: 'ko', msg: 'Impossible to create the first admin account'});
        }
    });
}

/** */
module.exports.modifyPassword = function(newPassword, res, tmp) {
    tmp.exec(function (err, count) {
        if (err) {
            return res.status(400).send({success: 'ok', msg: 'Problem on ID user'});
        } else {
            bcrypt.genSalt(10, function(err, salt) {
                bcrypt.hash(newPassword, salt, function(err, hash) {
                    count.password = hash;
                    count.save();
                    res.status(200).send({success: 'ok', msg: 'Password has been modified'});
                });
            });
        }
    });
}

/** */
module.exports.modifyEmail = function(newEmail, res, tmp) {
    tmp.exec(function(err, count) {
        if (err) {
            return res.status(400).send({success: 'ok', msg: 'Problem on ID user'});
        } else {
            count.email = newEmail;
            count.save();
            res.status(200).send({success: 'ok', msg: 'Email has been modified'});
        }
    });
}

/** */
module.exports.modifyAllInformation = function(newEmail, newPassword, newPermission, res, tmp) {
    tmp.exec(function(err, count) {
        if (err) {
            return res.status(400).send({success: 'ok', msg: 'Problem on ID user'});
        } else {
            count.email = newEmail;
            count.permission = newPermission;
            bcrypt.genSalt(10, function(err, salt) {
                bcrypt.hash(newPassword, salt, function(err, hash) {
                    if (err) {
                        return res.status(400).send({success: 'ok', msg: 'Problem on bcrypt'});
                    } else {
                        count.password = hash;
                        count.save();
                        res.status(200).send({success: 'ok', msg: 'Information has been modified'});
                    }
                });
            });
        }
    });
}

module.exports.modifyPermission = function(newPermission, res, tmp) {
    tmp.exec(function(err, count) {
        if (err || count == null) {
            return res.status(400).send({success: 'ok', msg: 'Problem on ID user'});
        } else {
            count.permission = newPermission;
            count.save();
            res.status(200).send({success: 'ok', msg: 'Permission modify'});
        }
    });
}

module.exports.getUsers = function(req, res) { 
    User.find({}).exec(function(err, count) {
        var table = [];
        var ct = 0;
        while (ct != count.length) {
            var tmp = {email: count[ct].email, admin: count[ct].permission, id: count[ct]._id};
            table.push(tmp);
            ct += 1;
        }
        res.status(200).send({success: 'ok', msg: table});
    });
}
