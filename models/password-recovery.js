var nodemailer = require('nodemailer');
var mongoose = require('mongoose');
const User = require('./user');
var Schema = mongoose.Schema;

var accountSupport = "smarthub.sup@gmail.com";
var passwordSupport = "mLvNDg9wN9";

var PasswordRecoverySchema = new mongoose.Schema({
    email: String,
    idUser: {type: Schema.Types.ObjectId, ref: User},
    dateEnd: Date
});

var PasswordRecovery = module.exports = mongoose.model('passwordRecovery', PasswordRecoverySchema);


module.exports.SendMailToUser = function(idUser, Email) {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: accountSupport, // generated ethereal user
            pass: passwordSupport // generated ethereal password
        }
    });
    //var link = linkRecoveryPassword + idUser;
    // send mail with defined transport object
    transporter.sendMail({
        from: 'accountSupport',
        to: Email, // list of receivers
        subject: 'Récupération mot de passe Smarthub', // Subject line
        text: 'Bonjour,\n\n' + 'Vous avez recemment fait une demande de changement de mot passe'
        + ' pour votre compte. Veuillez utiliser le code suivant pour finaliser le changement :\n' + idUser
        //html: '<b>Bonjour,</b>' + '<br><b>\nVous avez recemment fait une demande de changement de mot passe'
        //+ ' pour votre compte. Veuillez suivre ce lien pour finaliser le changement :</b><br>' + 
        //'<a href= {{link}}> link </a>'  // html body
    });
}

module.exports.StartRecoveryPassword = function(id, email, res) {
    var tmp = new Date();
    var newRecoveryPassword = new PasswordRecovery({
        email: email,
        idUser: id,
        dateEnd: new Date(tmp.setHours(tmp.getHours() + 1))
    });
    newRecoveryPassword.save();
    if (newRecoveryPassword) {
        module.exports.SendMailToUser(newRecoveryPassword._id, email);
        res.status(200).send({success: 'ok', msg: 'Recovery email send'});
    } else
        res.status(500).send({success: 'ko', msg: 'Cannot read password id'});
}

module.exports.EndRecoveryPassword = function(id, newPassword, res) {
    var tmp = PasswordRecovery.findOne({_id: id});
    tmp.exec(function(err, count) {
        if (err) {
            return res.status(409).send({success: 'ko', msg: 'Wrong Id'});
        }
        var tmpUser = User.findOne({_id: count.idUser});
        count.remove();
        return User.modifyPassword(newPassword, res, tmpUser);
    });
}