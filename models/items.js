var mongoose = require('mongoose');
var fs = require('fs');
var formidable = require('formidable');

var ItemSchema = new mongoose.Schema({
    name: String,
    imagePath: String,
    description: String,
    totalQuantity: Number,
    currentQuantity: Number
});

var Item = module.exports = mongoose.model('Item', ItemSchema);

module.exports.addItems = function(req, res) {
    var imagePath = '';
    if (req.file !== undefined) {
        imagePath = 'image/' + req.file.originalname.replace(' ', '_').toLowerCase();
    }
    var tmp = Item.find({name: req.body.name});
    tmp.exec(function(err, count) {
        if (count.length == 0) {
            var newItem = new Item({
                name: req.body.name,
                imagePath: imagePath,
                description: req.body.description,
                totalQuantity: req.body.quantity,
                currentQuantity: req.body.quantity
            });
            newItem.save();
            res.status(200).send({success: 'ok', msg: newItem._id});
        } else {
            res.status(409).send({success: 'ko', msg: 'This item already exist'});
        }
    })
}

module.exports.addImage = function(req, res) {
    Item.findOne({_id: req.params.id}).exec(function(err, count) {
        if (err || count == null) {
            return res.status(400).send({success: 'ko', msg: 'Wrong ID'});
        } else {
            var form = new formidable.IncomingForm();
            form.parse(req, function (err, fields, files) {
                var oldpath = files.file.path;
                var newpath = "image/" + files.file.name;
                fs.rename(oldpath, newpath, function (err) {
                    if (err) {
                        return res.status(400).send({success: 'ko', msg: 'Problem on upload'});
                    } else {
                        count.imagePath = files.file.name;
                        count.save();
                        return res.status(200).send({success: 'ok', msg: count});
                    }
                });
            });
        }
    });
}


VerifyItemPack = function(idItem, newQuantity) {
    var Pack = mongoose.model('pack');
    Pack.find({items: {$all: [idItem]}}).exec(function(err, count) {
        if (err || count.length == 0) {
            return;
        }
        var ct = 0;
        while (ct != count.length) {
            var ct2 = 0;
            while (ct2 != count[ct].items.length && ct2 != count[ct].quantity.length) {
                if (count[ct].items[ct2].equals(idItem) && count[ct].quantity[ct2] > newQuantity) {
                    count[ct].quantity[ct2] = newQuantity;
                    count[ct].markModified('quantity');
                    count[ct].warning[ct2] = true;
                    count[ct].markModified('warning');
                    count[ct].save();
                }
                ct2 += 1;
            }
            ct += 1;
        }
    });
}

module.exports.modifyItem = function(req, res) {
    var tmp = Item.findOne({_id: req.params.id});
    tmp.exec(function(err, count) {
        if (err || count == null) {
            return res.status(400).send({success: 'ko', msg: 'Wrong ID'});
        } else if (req.body.quantity < count.currentQuantity) {
            return res.status(400).send({success: 'ko', msg: 'Bad quantity'});
        } else {
            count.name = req.body.newName;
            count.description = req.body.newDescription;
            if (count.totalQuantity <= req.body.newQuantity) {
                count.currentQuantity += req.body.newQuantity - count.totalQuantity;
            } else {
                VerifyItemPack(count._id, req.body.newQuantity)
                if (count.currentQuantity > req.body.newQuantity) {
                    count.currentQuantity = req.body.newQuantity;
                }
            }
            count.totalQuantity = req.body.newQuantity;
            count.save();
            return res.status(200).send({success: 'ok', msg: count});
        }
    });
}

prepareResult = function(ct, count, data, res) {
    if (ct == count.length) {
        return res.status(200).send({success: 'ok', msg: data});
    } else {
        var tmp = {
            name: count[ct].name,
            description: count[ct].description,
            totalQuantity: count[ct].totalQuantity,
            currentQuantity: count[ct].currentQuantity,
            id: count[ct]._id,
            image: count[ct].imagePath
        };
        data.push(tmp);
        prepareResult(ct + 1, count, data, res)
    }
}

module.exports.getItems = function(res) {
    var tmp = Item.find({});
    tmp.exec(function(err, count) {
        var data = [];
        prepareResult(0, count, data, res);
    })
}

eraseItemBorrow = function(idItem) {
    var ItemBorrow = mongoose.model('itemBorrow');
    ItemBorrow.find({
          itemInfo: {$all: [idItem]}
    }).exec(function(err, data) {
        if (err || data.length == 0) {
            return;
        } else {
            var ct = 0;
            while (ct != data.length) {
                var ct2 = 0;
                while (ct2 != data[ct].itemInfo.length) {
                    if (data[ct].itemInfo[ct2] == idItem) {
                        data[ct].itemInfo.splice(ct2, 1);
                        data[ct].quantity.splice(ct2, 1);
                        data[ct].save();
                        break;
                    }
                    ct2 += 1;
                }
                ct += 1;
            }
        }
    });
}

eraseItemInPack = function(idItem) {
    var Pack = mongoose.model('pack');
    Pack.find({
        items: {$all: [idItem]}
    }).exec(function(err, data) {
        if (err || data.length == 0) {
            return;
        } else {
            var ct = 0;
            while (ct != data.length) {
                var ct2 = 0;
                while (ct2 != data[ct].items.length) {
                    if (data[ct].items[ct2] == idItem) {
                        data[ct].items.splice(ct2, 1);
                        data[ct].quantity.splice(ct2, 1);
                        data[ct].warning.splice(ct2, 1);
                        data[ct].save();
                        break;
                    }
                    ct2 += 1;
                }
                ct += 1;
            }
        }
    })
}

module.exports.eraseItems = function(IdItemTable, res) {
    var ct = 0;
    while (ct != IdItemTable.length) {
        Item.findOne({_id: IdItemTable[ct]}).exec(function(err, count) {
            if (count && count.imagePath !== undefined) {
                fs.unlink(count.imagePath, function(err) {}
            );
        }});
        Item.deleteOne({_id: IdItemTable[ct]}).exec();
        eraseItemBorrow(IdItemTable[ct]);
        eraseItemInPack(IdItemTable[ct]);
        ct += 1;
    }
    res.status(200).send({success: 'ok', msg: 'All items has been erased'});
}

module.exports.borrowItems = function(id, quantity) {
    Item.findOne({_id: id}).exec(function(err, count) {
        if (count !== null) {
            count.currentQuantity -= quantity;
            count.save();
        }
    });
}

module.exports.givaBackItems = function(id, quantity) {
    var tmp = Item.findOne({_id: id});
    tmp.exec(function(err, count) {
        if (!err && count !== null) {
            count.currentQuantity += quantity;
            if (count.currentQuantity > count.totalQuantity) {
                count.currentQuantity = count.totalQuantity;
            }
            count.save();
        }
    })
}

module.exports.verifyItems = function(req, res, ct, callback) {
    Item.find({
        _id: req.body.items[ct].id,
        totalQuantity: {$gte: req.body.items[ct].quantity}
    }).exec(function(err, count) {
        if (err || count.length == 0) {
            callback(true, req, res, ct)
        } else {
            callback(false, req, res, ct + 1);
        }
    });
}