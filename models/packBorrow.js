var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Pack = require('./pack');
const User = require('./user');
const Item = require('./items');
const { count } = require('./items');

const NotBorrow = 0;
const InProgress = 1;
const AlreadyBorrow = 2;

var PackBorrowSchema = new mongoose.Schema({
    name: String,
    idPack: {type: Schema.Types.ObjectId, ref: Pack},
    idUser: {type: Schema.Types.ObjectId, ref: User},
    dateStart: Date,
    dateEnd: Date,
    State: Number
});

var PackBorrow = module.exports = mongoose.model('packBorrow', PackBorrowSchema);

module.exports.ReserveOnePack = function(InformationRequest, res) {
    if (InformationRequest.err.err == true) {
        res.status(400).send({success: 'ko', msg: InformationRequest.err.msg})
    } else {
        Pack.findOne({
            _id: InformationRequest.idPack
        }).exec(function(err, count) {
            if (err || count == null) {
                return res.status(400).send({success: 'ko', msg: 'error on exec'});
            } else if (count.user !== InformationRequest.user._id && InformationRequest.user.permission == false) {
                return res.status(401).send({success: 'ko', msg: 'Don \'t have permission'});
            } else {
                var newPackBorrow = new PackBorrow({
                    name: InformationRequest.name,
                    idPack: count._id,
                    idUser: count.user,
                    dateStart: InformationRequest.startDate,
                    dateEnd: InformationRequest.endDate,
                    State: NotBorrow
                });
                newPackBorrow.save();
                return res.status(200).send({success: 'ok', msg: {id: newPackBorrow._id}});
            }
        });
    }
}

module.exports.ModifyOneReservePack = function(InformationRequest, res) {
    PackBorrow.findOne({
        _id: InformationRequest.idReserve,
        State: 3
    })
    .exec(function(err, count) {
        if (err || count == null) {
            return res.status(400).send({success: 'ko', msg: 'Wrong id'});
        } else if (InformationRequest.err.err == true) {
            count.State = 0;
            count.save();
            res.status(400).send({success: 'ko', msg: InformationRequest.err.msg})
        } else if (count.idUser !== InformationRequest.user._id && InformationRequest.user.permission == false) {
            count.State = 0;
            count.save();
            res.status(401).send({success: 'ko', msg: 'Don \'t have permission'});
        } else {
            count.name = InformationRequest.name;
            count.dateStart = InformationRequest.startDate;
            count.dateEnd = InformationRequest.endDate;
            count.State = 0;
            count.idPack = InformationRequest.idPack;
            count.save();
            res.status(200).send({success: 'ok', msg: count});
        }
    });
}

module.exports.ModifyNameOfOneReserverPack = function(name, id, user, res) {
    PackBorrow.findOne({
        _id: id,
        State: NotBorrow
    }).populate('idPack')
    .exec(function(err, data) {
        if (err) {
            res.status(400).send({success: 'ko', msg: err})
        } else if (data == null) {
            return res.status(400).send({success: 'ko', msg: 'Wrong id'});
        } else if (count.idUser !== user._id && user.permission == false) {
            return res.status(401).send({success: 'ko', msg: 'Don \'t have permission'});
        } else {
            data.name = name;
            data.save();
            res.status(200).send({success: 'ok', msg: data});
        }
    })
}

module.exports.EraseOneReservePack = function(id, user, res) {
    PackBorrow.findOne({
        _id: id,
         State: NotBorrow
    }).exec(function(err, count) {
        if (err || count == null) {
            return res.status(400).send({success: 'ko', msg: 'Wrong id'});
        } else if (count.user !== user._id && user.permission == false) {
            return res.status(401).send({success: 'ko', msg: 'Don \'t have permission'});
        } else {
            count.remove();
            res.status(200).send({success: 'ok', msg: 'Pack remove'});
        }
    });
}

module.exports.getInformationOfBorrowPack = function(tmp, res) {
    tmp.exec(function(err, count) {
        if (err) {
            return res.status(400).send({success: 'ko', msg: 'error on exec'});
        } else {
            var result = [];
            var ct = 0;
            while (ct != count.length) {
                var tmp = {};
                tmp.id = count[ct]._id;
                tmp.Pack = count[ct].idPack;
                tmp.name = count[ct].name;
                tmp.email = count[ct].idUser.email;
                tmp.startDate = count[ct].dateStart;
                tmp.endDate = count[ct].dateEnd;
                result.push(tmp);
                ct += 1;
            }
            return res.status(200).send({success: 'ok', msg: result});
        }
    });
}

module.exports.getAllBorrowPack = function(res) {
    var tmp = PackBorrow.find({}).populate({path: 'idPack', populate: {path: 'items', model: Item}}).populate('idUser');
    return module.exports.getInformationOfBorrowPack(tmp, res);
}

module.exports.getAccountBorrowPack = function(req, res) {
    if (req.params.id !== req.user._id && req.user.permission == false) {
        return res.status(401).send({success: 'ko', msg: 'Permission denied'});
    }
    var tmp = PackBorrow.find({idUser: req.user._id}).populate('idPack');
    return module.exports.getInformationOfBorrowPack(tmp, res);
}

module.exports.verifyStartBorrowing = function() {
    var tmp = PackBorrow.find({
        dateStart: {$lte: Date()},
        State: NotBorrow
    }).populate('idPack')
    .exec(function(err, count) {
        if (err || count.length == 0) {
            return;
        } else {
            var ct = 0;
            while (ct != count.length) {
                var ct2 = 0;
                while (ct2 != count[ct].idPack.items.length && ct2 != count[ct].idPack.quantity.length) {
                    Item.borrowItems(count[ct].idPack.items[ct2], count[ct].idPack.quantity[ct2]);
                    ct2 += 1;
                }
                count[ct].State = InProgress;
                count[ct].save();
                ct += 1;
            }
        }
    })
};

module.exports.verifyEndBorrowing = function() {
    var tmp = PackBorrow.find({
        dateEnd: {$lte: Date()},
        State: InProgress
    }).populate('idPack')
    .exec(function(err, count) {
        if (err || count.length == 0) {
            return;
        } else {
            var ct = 0;
            while (ct != count.length) {
                var ct2 = 0;
                while (ct2 != count[ct].idPack.items.length && ct2 != count[ct].idPack.quantity.length) {
                    Item.givaBackItems(count[ct].idPack.items[ct2], count[ct].idPack.quantity[ct2]);
                    ct2 += 1;
                }
                count[ct].State = AlreadyBorrow;
                count[ct].save();
                ct += 1;
            }
        }
    })
};

setInterval(PackBorrow.verifyStartBorrowing, 60000);
setInterval(PackBorrow.verifyEndBorrowing, 60000);