const express = require('express');
const router = express.Router();
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;

const ensure = require('../js/protect');
const Pack = require('../models/pack');
const BorrowPack = require('../models/packBorrow');
const verificationPackBorrow = require('../js/verificationPackBorrow');

/**
 * @route localhost:8040/api/pack/create (POST)
 * @resume Permet de créer un pack associé au compte courant (nécessité de s'être connecté
 * en premier lieu)
 * 
 * @param name nom du pack
 * @param items tableau contenant la liste de tous les items prévues dans le pack
 * @param items.id id de l'item
 * @param items.quantity nom d'items
 */
router.post('/create', [
    check('name', 'Your name is not valid').not().isEmpty(),
    check('items', 'Items array must not be empty').isArray().custom((value) => value.length != 0),
    check('items.*.id', 'Id is necessery in items array').not().isEmpty(),
    check('items.*.quantity', 'Quantity is necessery in items array (> 0)').not().isEmpty().isInt({min: 0}),
], ensure.authenticated,
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        return Pack.createPack(false, req, res, 0);
    }
});

/**
 * @route localhost:8040/api/pack/:id (PUT)
 * @resume Permet de modifié un pack associé au compte spécifié en paramètre
 *  (nécessité de s'être connecté en premier lieu)
 * 
 * @param name nom du pack
 * @param items tableau contenant la liste de tous les items prévues dans le pack
 * @param items.id id de l'item
 * @param items.quantity nom d'items
 */
router.put('/:id', [
    check('name', 'Your name is not valid').not().isEmpty(),
    check('items', 'Items array must not be empty').isArray().custom((value) => value.length != 0),
    check('items.*.id', 'Id is necessery in items array').not().isEmpty(),
    check('items.*.quantity', 'Quantity is necessery in items array (> 0)').not().isEmpty().isInt({min: 0}),
], ensure.authenticated, 
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        return Pack.modifyPack(false, req, res, 0);
    }
});

/**
 * @route localhost:8040/api/pack/:id (DELETE)
 * @resume Permet d'effacer un pack associé au compte spécifié en paramètre
 *  (nécessité de s'être connecté en premier lieu)
 */
router.delete('/:id', ensure.authenticated, function(req, res) {
    return Pack.erasePack(req.params.id, req, res);
});

/**
 * @route localhost:8040/api/pack (GET)
 * @resume Permet de récupérer la liste de tous les packs stockées dans la base de donnée
 * (nécessité de s'être connecté et d'avoir les droits d'aministrateur)
 */
router.get('/', ensure.adminRights, function(req, res) {
    return Pack.getAllPack(res);
});

/**
 * @route localhost:8040/api/pack/user/:id (GET)
 * @resume Permet de récupérer la liste de tous les packs stockées dans la base de donnée
 * (nécessité de s'être connecté et d'avoir les droits d'administrateur)
 */
router.get('/user/:id', ensure.authenticated, function(req, res) {
    return Pack.getPack(req, res);
});

/**
 * @route localhost:8040/api/pack/reserve/:id (POST)
 * @resume Permet de réserver un pack à une certaine date (nécessité de s'être connecté avant)
 * 
 * @param startDate Date du début de la réservation
 * @param endDate Date de la fin de la réservation
 */
router.post('/reserve/:id', [
    check('name', 'Event name is necessary').not().isEmpty(),
    check('startDate', 'Start Date is necessery').not().isEmpty().isISO8601(),
    check('endDate', 'End Date is necessery').not().isEmpty().isISO8601(),
], ensure.authenticated, function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        var tmp = new Date();
        tmp.setHours(tmp.getHours() + 24);
        if (req.body.startDate > req.body.endDate) {
            return res.status(400).send({success: 'ko', msg: 'Wrong Date'})
        } else if (req.body.startDate < tmp.toISOString()) {
            return res.status(400).send({success: 'ko', msg: 'You must reserve a pack at least 24 hours in advance'});
        } else {
            var InformationRequest = {
                idPack: req.params.id,
                name: req.body.name,
                startDate: new Date(req.body.startDate),
                endDate: new Date(req.body.endDate),
                user: req.user,
                err: {err: false, msg: ""}
            };
            verificationPackBorrow.createInformationListItem(InformationRequest, res, BorrowPack.ReserveOnePack);
        }
    }
});

/**
 * @route localhost:8040/api/pack/borrow (GET)
 * @resume Permet de récupérer la liste de tous les packs ayant un emprunt prévu 
 */
router.get('/borrow', ensure.authenticated, function(req, res) {
    return BorrowPack.getAllBorrowPack(res);
});

/**
 * @route localhost:8040/api/pack/borrow/user/:id (GET)
 * @resume Permet de récupérer la liste de tous les packs ayant un emprunt prévu, appartenant 
 * à l'user spécifié en paramètre
 */
router.get('/borrow/user/:id', ensure.authenticated, function(req, res) {
    return BorrowPack.getAccountBorrowPack(req, res)
});

fillInformationRequest = function(newName, newDateStart, newDateEnd, InformationRequest, res, data) {
    var tmp = new Date();
    tmp.setHours(tmp.getHours() + 24);
    if (newName != undefined) {
        InformationRequest.name = newName
    } 
    if (newDateStart != undefined && newDateEnd != undefined) {
        if (newDateStart > newDateEnd) {
            res.status(400).send({success: 'ko', msg: 'Wrong Date'})
        } else if (newDateStart < tmp.toISOString()) {
            res.status(400).send({success: 'ko', msg: 'You must reserve a pack at least 24 hours in advance'});
        } else {
            InformationRequest.startDate = newDateStart;
            InformationRequest.endDate = newDateEnd;
            data.State = 3;
            data.save();
            verificationPackBorrow.createInformationListItem(InformationRequest, res, BorrowPack.ModifyOneReservePack);
        }
    } else if (newDateStart != undefined) {
        if (new Date(newDateStart) > InformationRequest.endDate) {
            res.status(400).send({success: 'ko', msg: 'Wrong Date'})
        } else if (newDateStart < tmp.toISOString()) {
            res.status(400).send({success: 'ko', msg: 'You must reserve a pack at least 24 hours in advance'});
        } else {
            InformationRequest.startDate = newDateStart;
            data.State = 3;
            data.save();
            verificationPackBorrow.createInformationListItem(InformationRequest, res, BorrowPack.ModifyOneReservePack);
        }
    } else if (newDateEnd != undefined) {
        if (InformationRequest.startDate > new Date(newDateEnd)) {
            res.status(400).send({success: 'ko', msg: 'Wrong Date'})
        } else {
            InformationRequest.endDate = newDateEnd;
            data.State = 3;
            data.save();
            verificationPackBorrow.createInformationListItem(InformationRequest, res, BorrowPack.ModifyOneReservePack);
        }
    }
}

router.put('/borrow/:id', [
    check('newName', 'Name is required').optional(),
    check('newDateStart', 'Date Start is required').optional().isISO8601(),
    check('newDateEnd', 'Date End is required').optional().isISO8601(),
    check('newIdPack').optional()
],
ensure.authenticated,
function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        if (req.body.newName != undefined && req.body.newDateStart == undefined && req.body.newDateEnd == undefined) {
            BorrowPack.ModifyNameOfOneReserverPack(req.body.newName, req.params.id, req.user, res);
        } else {
            BorrowPack.findOne({
                _id: req.params.id,
                State: 0
            })
            .exec(function(err, data) {
                if (err) {
                    res.status(400).send({success: 'ko', msg: 'err'});
                } else if (data == null) {
                    res.status(400).send({success: 'ko', msg: 'Error on id'});
                } else {
                    var InformationRequest = {
                        idReserve: data._id,
                        idPack: data.idPack,
                        name: data.name,
                        startDate: data.dateStart,
                        endDate: data.dateEnd,
                        user: req.user,
                        err: {err: false, msg: ""}
                    };
                    if (req.body.newIdPack == undefined) {
                        fillInformationRequest(req.body.newName, req.body.newDateStart, req.body.newDateEnd, InformationRequest, res, data);
                    } else {
                        InformationRequest.idPack = req.body.newIdPack;
                        fillInformationRequest(req.body.newName, req.body.newDateStart, req.body.newDateEnd, InformationRequest, res, data);
                    }
                }
            })
        }
    }
});


router.delete('/borrow/:id', ensure.authenticated, function(req, res) {
    return BorrowPack.EraseOneReservePack(req.params.id, req.user, res);
});

module.exports = router;