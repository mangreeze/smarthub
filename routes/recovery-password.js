const express = require('express');
const router = express.Router();
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;

var User = require('../models/user');
var PasswordRecovery = require('../models/password-recovery');

router.post('/', [
    check('email', 'email is required').not().isEmpty().isEmail()
], 
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json(errors.array({onlyFirstError: true}))
    } else {
        User.getUserByEmail(req.body.email, function(err, result) {
            if (err || !result) {
                return res.status(409).send({success: 'ko', msg: 'This email doesn \'t exist'});
            }
            return PasswordRecovery.StartRecoveryPassword(result._id, req.body.email, res);
        })
    }
});

router.put('/password/:id', [
    check('password', 'Your password must be at least 5 characters').not().isEmpty().isLength({min: 5}),
    check('confirmPassword', 'Password do not match').custom((value, {req}) => (value === req.body.password)),
],
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        return PasswordRecovery.EndRecoveryPassword(req.params.id, req.body.password, res);
    }
});

module.exports = router