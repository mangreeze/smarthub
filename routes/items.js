const express = require('express');
const router = express.Router();
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;
const ensure = require('../js/protect');

const Items = require('../models/items');
const VerificationItemBorrow = require('../js/verificationItemBorrow');

/**
 * @route localhost:8040/api/items/register (POST)
 * @resume Permet de rajouter un objet dans la base de donnée smarthub
 * 
 * @param name nom de l'objet (doit être non vide)
 * @param description description de l'objet
 * @param quantity nombre d'objet à ajouter (doit être supérieur à 0)
 */
router.post('/register', [
    check('name', 'Name is required').not().isEmpty(),
    check('description', 'Description is required').not(),
    check('quantity', 'Quantity is required').not().isEmpty().isInt({min: 0})
], ensure.adminRights
, function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        return Items.addItems(req, res);
    }
});

/**
 * @route localhost:8040/api/items/:id (PUT)
 * @resume Permet de modifier un objet déjà existant dans la base de donnée smarthub
 * 
 * @param newName nouveau nom de l'objet
 * @param newDescription nouvelle description de l'objet
 * @param newQuantity nouvelle quantité maximale de l'objet
 */
router.put('/:id', [
    check('newName', 'Name is required').not().isEmpty(),
    check('newDescription', 'Description is required').not(),
    check('newQuantity', 'Quantity is required').not().isEmpty().isInt({min: 0})
], ensure.adminRights
, function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        return Items.modifyItem(req, res);
    }
});

/**
 * @route localhost:8040/api/items/:id/image
 * @resume Permet d'ajouter une image à un item déja enregisté dans la base de donnée
 */
router.put('/:id/image', ensure.adminRights, function(req, res) {
    return Items.addImage(req, res)
})

/**
 * @route localhost:8040/api/items (GET)
 * @resume permet de récupérer l'ensemble des objets contenues dans la base de donnée smarthub
 */
router.get('/', function(req, res) {
    return Items.getItems(res);
});

/**
 * @route localhost:8040/api/items (DELETE)
 * @resume Permet de supprimer plusieurs items de la base de donnée smarthub
 * 
 * @param items Tableau contenant l'id de tous les objets à effacer
 * 
 * A FAIRE: effacer aussi dans les items empruntée et les packs !!!!!!!!!!!!!!!!!
 */
router.delete('/', [
    check('items', 'Items array must be not empty').isArray().custom((value) => value.length != 0),
], ensure.adminRights,
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        return Items.eraseItems(req.body.items, res);
    }
});

/**
 * @route localhost:8040/api/items/borrow (POST)
 * @resume Permet d'emprunter une liste d'items pour une certaine durée
 * 
 * @param items Tableau contenant les informations sur les items empruntés (id: contient l'ID de l'item, quantity: contient la quantité de l'item a emprunté)
 * @param duration Temps pour lequels les items sont empruntés (en HEURE)
 */
router.post('/borrow', [
    check('items', 'Items array must not be empty').isArray().custom((value) => value.length != 0),
    check('items.*.id', 'Id is necessery in items array').not().isEmpty(),
    check('items.*.quantity', 'Quantity is necessery in items array (> 0)').not().isEmpty().isInt({min: 0}),
    check('duration', 'Duration is necessery').not().isEmpty().isInt({min: 0})
], 
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        var InformationListItem = [];
        VerificationItemBorrow.startVerification(false, req.body.items, req.body.duration
            , InformationListItem, 0, res);
    }
});

module.exports = router;