const express = require('express');
const router = express.Router();
const session = require('express-session');
const check = require('express-validator/check').check;
const validationResult = require('express-validator/check').validationResult;
var passport = require('passport');

const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');

const ensure = require('../js/protect');

/**
 * @route localhost:8040/api/users/register (POST)
 * @resume Permet la création d'un nouveau compte
 * 
 * @param email l'email du compte (doit être non vide)
 * @param password le mot de passe du compte (doit contenir au moins 5 caractères)
 * @param confirmPassword mot de passe de confirmation (doit être identique au password)
 * @param permission bool (true -> si posséde les droits admin, false -> sinon)
 */
router.post('/register', [
    check('email', 'Your mail is not valid').not().isEmpty().isEmail(),
    check('password', 'Your password must be at least 5 characters').not().isEmpty().isLength({min: 5}),
    check('confirmPassword', 'Password do not match').custom((value, {req}) => (value === req.body.password)),
    check('permission', 'Add the permission').not().isEmpty().isBoolean(),
], ensure.adminRights, 
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        return User.createUser(req, res);
    }
});

/**
 * @fonction Permet de paramètrer le module passport (NE PAS TOUCHER)
 */
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},
    function(email, password, done) {
        User.getUserByEmail(email, function(err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, {message: 'Unknown Email'});
            }
            User.comparePassword(password, user.password, function(err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    user.token =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                    user.save();
                    return done(null, user)
                } else {
                    return done(null, false, {message: 'Invalid password'});
                }
            });
        });
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
});

/**
 * @route localhost:8040/api/users/login (GET)
 * @resume Permet de connecter à un compte smarthub
 * 
 * @param email email du compte
 * @param password mot de passe du compte
 */
router.post('/login', [
    check('email', 'Your mail is not valid').not().isEmpty().isEmail(),
    check('password', 'Your password must be at least 5 characters').not().isEmpty().isLength({min: 5}),
],
function(req, res, next) {
    passport.authenticate('local', function (err, user) {
        req.logIn(user, function() {
            if (!user) {
                res.status(400).send({success: 'ko', msg: "Invalid Email or Password"});
            } else {
                req.session.idUser = user._id;
                res.status(200).send({success: 'ok', msg: {email: user.email, admin: user.permission, id: user._id, token: user.token}});
            }
        })
    })(req, res, next);
});

/**
 * @route localhost:8040/api/users/login (GET)
 * @resume Permet de se déconnecter du serveur smarthub
 */
router.get('/logout', ensure.authenticated, function(req, res) {
    req.user.token = "";
    req.user.save();
    req.logout();
    return res.status(200).send({success: 'ok', msg: 'you are logout'});
});

/**
 * @route localhost:8040/api/users/admin
 * @resume Permet de créer le 1er compte admin de l'application (ne peut être utilisé que
 * si la base de donnée smarthub est vide)
 * 
 * @param email email du compte
 * @param password mot de passe du compte
 * @param confirmPassword mot de passe de confirmation (doit être identique au password)
 */
router.post('/admin', [
    check('email', 'Your email is not valid').not().isEmpty().isEmail(),
    check('password', 'Your password must be at least 5 characters').not().isEmpty().isLength({min: 5}),
    check('confirmPassword', 'Password do not match').custom((value, {req}) => (value === req.body.password)),
],
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        User.createFirstAdminAccount(req, res);
    }
});

/**
 * @route localhost:8040/api/users/:id
 * @resume Permet de modifier les informations d'un compte spécifié en paramètre, 
 * c.a.d l'email et le mot de passe (nécessité de s'être dans un premier temps connectée avec la route login)
 * 
 * @param newEmail nouveau email du compte
 * @param newPassword nouveau password du compte
 * @param newConfirmPassword mot de passe de confirmation (doit être identique au newPassword)
 */
router.put('/:id', [
    check('newEmail', 'Your email is not valid').not().isEmpty().isEmail(),
    check('newPassword', 'Your password must be at least 5 characters').not().isEmpty().isLength({min: 5}),
    check('newConfirmPassword', 'Password do not match').custom((value, {req}) => (value === req.body.newPassword)),
], ensure.authenticated,
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else if (req.params.id !== req.user._id && req.user.permission == false) {
        return res.status(400).send({success: 'ko', msg: 'You don \'t have the permission'});
    } else {
        var tmp = User.findById({_id: req.params.id});
        return User.modifyAllInformation(req.body.newEmail, req.body.newPassword, req.user.permission, res, tmp);
    }
});

/**
 * @route localhost:8040/api/users/:id/mail (PUT)
 * @resume Permet de modifier l'email du compte courant (nécessité de s'être dans un
 * premier temps connecté)
 * 
 * @param newEmail nouveau email du compte
 */
router.put('/:id/email', [
    check('newEmail', 'Your email is not valid').not().isEmpty().isEmail(),
], ensure.authenticated,
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else if (req.params.id !== req.user._id && req.user.permission == false) {
        return res.status(400).send({success: 'ko', msg: 'You don \'t have the permission'});
    } else {
        var tmp = User.findById({_id: req.params.id});
        return User.modifyEmail(req.body.newEmail, res, tmp);
    }
});

/**
 * @route localhost:8040/api/users/:id/password (PUT)
 * @resume Permet de modifier le mot de passe du compte courant (nécessité de s'être dans
 * un premier temps connecté)
 * 
 * @param newPassword nouveau mot de passe du compte
 * @param newConfirmPassword mot de passe de confirmation (doit être identique au newPassword)
 */
router.put('/:id/password', [
    check('newPassword', 'Your password must be at least 5 characters').not().isEmpty().isLength({min: 5}),
    check('newConfirmPassword', 'Password do not match').custom((value, {req}) => (value === req.body.newPassword)),
], ensure.authenticated,
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else if (req.params.id !== req.user._id && req.user.permission == false) {
        return res.status(400).send({success: 'ko', msg: 'You don \'t have the permission'});
    } else {
        var tmp = User.findById({_id: req.params.id});
        return User.modifyPassword(req.body.newPassword, res, tmp);
    }
});

/**
 * @route localhost:8040/api/users/:id/permission (PUT)
 * @resume Permet de modifier la permission du compte spécifié en paramètre (nécessité de s'être dans
 * un premier temps connecté)
 * 
 * @param newPermission nouvelle permission
 */
router.put('/:id/permission', [
    check('newPermission', 'Permission is required').not().isEmpty().isBoolean()
], ensure.adminRights, 
function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).jsonp(errors.array({onlyFirstError: true}));
    } else {
        var tmp = User.findById({_id: req.params.id});
        return User.modifyPermission(req.body.newPermission, res, tmp);
    }
});

/**
 * @route localhost:8040/api/users (GET)
 * @resume Permet de récupérer la liste de tous les comptes déja crée
 */
router.get('/', function(req, res) {
    User.getUsers(req, res);
});

module.exports = router;